"use strict";

//noinspection JSUnusedLocalSymbols
var _ = require("lodash");
import {messageRestClient} from "../../services/rest/rest-client.jsx";
import MessageActions from "./message-actions.jsx";
import bamboostFlux from "../../bamboost-flux.jsx";
import {MESSAGE_GROUPS} from "./message-store.jsx";

//noinspection JSUnusedLocalSymbols
export default (alt) => ({
    fetchMessage(id, forceFetch) {
        //noinspection JSUnusedLocalSymbols
        return {
            local(state,id, forceFetch){
                return forceFetch ? null : state.messages.getEntity(id);
            },
            remote(state, id) {
                return messageRestClient.fetchMessage(id);
            },
            success: alt.actions.messageActions.fetchedMessage,
            error: alt.actions.messageActions.messageError
        }
    },

    fetchComments(commentedPath){
        //noinspection JSUnusedLocalSymbols
        return {
            remote(state, commentedPath) {
                let lastMessage = state.messages[MESSAGE_GROUPS.comments].getLastEntity(commentedPath);
                let before = lastMessage ? new Date(lastMessage.creationMoment) : new Date();
                return messageRestClient.fetchComments(commentedPath, before);
            },
            interceptResponse(response, action, args){
                let callback = args[1];
                if (callback) callback();
                return response;
            },
            success: alt.actions.messageActions.fetchedComments,
            error: alt.actions.messageActions.messageError
        }
    },
    fetchOrganizationMessages(id, callback){
        //noinspection JSUnusedLocalSymbols
        return {
            remote(state, organizationId) {
                let lastMessage = state.messages[MESSAGE_GROUPS.organizationRoots].getLastEntity(organizationId);
                let before = lastMessage ? new Date(lastMessage.creationMoment) : new Date();
                return messageRestClient.fetchOrganizationMessages(organizationId, before);
            },
            interceptResponse(response, action, args){
                let callback = args[1];
                if (callback) callback();
                return response;
            },
            success: alt.actions.messageActions.fetchedOrganizationMessages,
            error: alt.actions.messageActions.messageError
        }
    },
    searchOrganizationMessages(searchTerm, organizationId, callback){
        //noinspection JSUnusedLocalSymbols
        return {
            local(state, searchTerm, organizationId){
                alt.actions.messageActions.resetSearch(organizationId);
                return null;
            },
            remote(state, searchTerm, organizationId) {
                return messageRestClient.searchMessages(searchTerm, organizationId);
            },
            interceptResponse(response, action, args){
                let callback = args[2];
                if (callback) callback();
                return response;
            },
            success: alt.actions.messageActions.fetchedSearchedMessages,
            error: alt.actions.messageActions.messageError
        }
    },
    searchMoreOrganizationMessages(searchTerm, organizationId, callback){
        //noinspection JSUnusedLocalSymbols
        return {
            remote(state, searchTerm, organizationId) {
                let offset = state.messages[MESSAGE_GROUPS.organizationSearched].count(organizationId);
                return messageRestClient.searchMessages(searchTerm, organizationId, offset);
            },
            interceptResponse(response, action, args){
                let callback = args[2];
                if (callback) callback();
                return response;
            },
            success: alt.actions.messageActions.fetchedSearchedMessages,
            error: alt.actions.messageActions.messageError
        }
    },
    deleteMessage(message){
        //noinspection JSUnusedLocalSymbols
        return {
            remote(state, message){
                alt.actions.messageActions.markPending(message.id);
                return messageRestClient.deleteMessage(message.id);
            },
            interceptResponse(response, action, args){
                let message = args[0];
                let parentId = message.parentId;
                if (parentId){
                    alt.stores.messageStore.fetchMessage(parentId, true);
                }
                return response;
            },
            success: alt.actions.messageActions.deletedMessage,
            error: alt.actions.messageActions.messageError
        }
    },
    postMessage(message){
        //noinspection JSUnusedLocalSymbols
        return {
            remote(state, message){
                let pendingId = messageRestClient.getPendingId();
                alt.actions.messageActions.postMessage({message, pendingId});
                return messageRestClient.postMessage({message, pendingId});
            },
            interceptResponse(response, action, args){
                let parentId = response.parentId;
                if (parentId){
                    alt.stores.messageStore.fetchMessage(parentId, true);
                }
                return response;
            },
            success: alt.actions.messageActions.fetchedMessage,
            error: alt.actions.messageActions.messageError
        }
    }
});