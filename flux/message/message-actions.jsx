"use strict";

import {messageRestClient} from "../../services/rest/rest-client.jsx";

export default class MessageActions {
    static displayName = 'MessageActions';

    //noinspection JSMethodCanBeStatic
    fetchedMessage(message){ return message; }

    //noinspection JSMethodCanBeStatic
    deletedMessage(messageId){return messageId;}

    //noinspection JSMethodCanBeStatic
    fetchedOrganizationMessages({organizationId, messages}){return {organizationId, messages};}

    //noinspection JSMethodCanBeStatic
    resetSearch(organizationId){return organizationId;}

    //noinspection JSMethodCanBeStatic
    fetchedSearchedMessages({organizationId, messages}){return {organizationId, messages};}

    //noinspection JSMethodCanBeStatic
    fetchedComments({commentedPath, messages}){return {commentedPath, messages};}

    //noinspection JSMethodCanBeStatic
    messageError(error){return error;}

    //noinspection JSMethodCanBeStatic
    postMessage(data){return data;}

    //noinspection JSMethodCanBeStatic
    markPending(messageId){return messageId;}
}