"use strict";
var _ = require('lodash');

import GroupedEntities from "../grouped-entities.jsx";

import MessageSource from './message-source.jsx';
import MessageActions from "./message-actions.jsx";
import bamboostFlux from "../../bamboost-flux.jsx";

export const MESSAGE_GROUPS = {
    comments: "rootsByCommented",
    organizationRoots: "rootsByOrganizationId",
    organizationSearched: "searchedByOrganizationId",
    replies: "byParentId"
};

export default class MessageStore {
    constructor() {
        this.messages = new GroupedEntities();
        this.messages.addGroup(MESSAGE_GROUPS.comments, "commentedPath", "creationMoment", false);
        this.messages.addGroup(MESSAGE_GROUPS.organizationRoots, "organizationId", "creationMoment", false);
        this.messages.addGroup(MESSAGE_GROUPS.organizationSearched, "organizationId");
        this.messages.addGroup(MESSAGE_GROUPS.replies, "parentId", "creationMoment");

        let messageActions = this.alt.getActions("messageActions");
        this.bindAction(messageActions.deletedMessage, this.onDeletedMessage);
        this.bindAction(messageActions.fetchedMessage, this.onFetchedMessage);
        this.bindAction(messageActions.fetchedOrganizationMessages, this.onFetchedOrganizationMessages);
        this.bindAction(messageActions.fetchedComments, this.onFetchedComments);
        this.bindAction(messageActions.resetSearch, this.onResetSearch);
        this.bindAction(messageActions.fetchedSearchedMessages, this.onFetchedSearchedMessages);
        this.bindAction(messageActions.messageError, this.onMessageError);
        this.bindAction(messageActions.postMessage, this.onPostMessage);
        this.bindAction(messageActions.markPending, this.onMarkPending);
        this.registerAsync(MessageSource);
    }

    onResetSearch(organizationId){
        this.messages[MESSAGE_GROUPS.organizationSearched].resetEntities(organizationId);
    }

    onMarkPending(messageId) {
        let message = this.messages.getEntity(messageId);
        message.isPending = true;
    }

    onDeletedMessage(messageId) {
        var message = this.messages.getEntity(messageId);
        this.messages.remove(message);
    }

    onFetchedMessage(message) {
        this.messages[MESSAGE_GROUPS.replies].ensureGrouped(message.id);
        this.messages.addEntity(message, [MESSAGE_GROUPS.replies]);
        if (!message.parentId) {
            this.messages[MESSAGE_GROUPS.organizationRoots].addIfGrouped(message);
            this.messages[MESSAGE_GROUPS.comments].addIfGrouped(message);
        }
        if (message.pendingId) {
            this.onDeletedMessage(message.pendingId);
            delete message.pendingId;
        } else if (message.replies) {
            this.onFetchedMessages(message.replies);
            delete message.replies;
        }

        var userStore = this.alt.stores.userStore;
        if (userStore.getUser(message.author.id) == null)
            userStore.fetchUser(message.author.id);
    }

    onFetchedMessages(messages) {
        messages.forEach((message)=>this.onFetchedMessage(message));
    }

    onPostMessage({message,pendingId}) {
        let pendingMessage = _.assign({}, message, {
            id: pendingId,
            isPending: true,
            creationMoment: Date.now(),
            author: this.alt.stores.userStore.getLoggedUser()
        });
        let groups = pendingMessage.parentId == null
            ? [MESSAGE_GROUPS.replies, MESSAGE_GROUPS.organizationRoots, MESSAGE_GROUPS.comments]
            : [MESSAGE_GROUPS.replies];
        this.messages.addEntity(pendingMessage, groups);
    }

    //noinspection JSMethodCanBeStatic
    onMessageError(err) {
        console.error(err)
    }

    onFetchedSearchedMessages({organizationId, messages}) {
        this.onFetchedMessages(messages);
        let searchResults = messages || [];
        this.messages[MESSAGE_GROUPS.organizationSearched].addEntitiesToGroup(searchResults, organizationId);
    }

    onFetchedOrganizationMessages({organizationId, messages}) {
        if (messages) {
            this.onFetchedMessages(messages);
            this.messages[MESSAGE_GROUPS.organizationRoots].addEntitiesToGroup(messages, organizationId);
        }
        else
            this.messages[MESSAGE_GROUPS.organizationRoots].ensureGrouped(organizationId);
    }

    onFetchedComments({commentedPath, messages}) {
        if (messages) {
            this.onFetchedMessages(messages);
            this.messages[MESSAGE_GROUPS.comments].addEntitiesToGroup(messages, commentedPath);
        }
        else
            this.messages[MESSAGE_GROUPS.comments].ensureGrouped(commentedPath);
    }
}