"use strict";

import { decorate, datasource, bind } from 'alt-utils/lib/decorators.js'
import UserSource from './user-source.jsx';
import UserActions from "./user-actions.jsx";
import bamboostFlux from "../../bamboost-flux.jsx";

import GroupedEntities from "../grouped-entities.jsx";


export default class UserStore {
    constructor() {
        this.users = new GroupedEntities();
        this.users.addGroup("byOrganizationId", "organizationIds");

        this.exportPublicMethods({
            getLoggedUser: this.getLoggedUser,
            ensureLoggedUserFetched: this.ensureLoggedUserFetched,
            getUser: this.getUser
        });

        this.bindAction(this.alt.actions.userActions.fetchedUser, this.onFetchedUser);
        this.bindAction(this.alt.actions.userActions.updateUser, this.onUpdateUser);
        this.bindAction(this.alt.actions.userActions.fetchedUsersInOrganization, this.onFetchedUsersInOrganization);
        this.bindAction(this.alt.actions.userActions.noUserFound, this.onNoUserFound);
        this.bindAction(this.alt.actions.userActions.notifyLogin, this.onNotifyLogin);
        this.registerAsync(UserSource);
    }

    getUser(userId){
        return this.getState().users.getEntity(userId);
    }

    ensureLoggedUserFetched(){
        let userId = this.alt.bamboostContext.loggedUserId;
        if (userId != null) {
            userId = parseInt(userId);
            if (!this.state.users.getEntity(userId))
                this.fetchUser(userId);
        }
    }

    getLoggedUser(){
        if (this.alt.bamboostContext.loggedUserId) {
            return this.getState().users.getEntity(this.alt.bamboostContext.loggedUserId);
        } else {
            return null;
        }
    }

    onFetchedUser(user) {
        if (user !== null) {
            this.users.addEntity(user);
            if (user.id == this.alt.bamboostContext.loggedUserId){
                this.alt.bamboostContext.loggedUser = user;
            }
        }
    }

    onFetchedUsersInOrganization({organizationId, users}){
        var storeUsers = this.users;
        storeUsers.addEntities(users);
        storeUsers['byOrganizationId'].addEntitiesToGroup(users, organizationId);
    }

    onNoUserFound(err) {
        console.error(err)
    }

    onNotifyLogin(userId) {
        console.debug("logged in");
    }

    onUpdateUser(userId) {
    }
}