"use strict";

import {userRestClient} from "../../services/rest/rest-client.jsx";

export default class UserActions {

    constructor() {
        this.generateActions(
            'noUserFound',
            'notifyLogin'
        )
    }

    //noinspection JSMethodCanBeStatic
    fetchedUser(user){return user;}

    //noinspection JSMethodCanBeStatic
    fetchedUsersInOrganization({organizationId, users}){
        return {organizationId, users}
    }

    updateUser(patch, callback) {
        userRestClient.patchUser(patch)
            .then((user)=> {
                callback();
                this.fetchedUser(user)
            })
            .catch((error)=>console.log('Error when trying to update user: ' + JSON.stringify(error)));
        return true;
    }

    uploadPortrait(userId, file, callback) {
        userRestClient.uploadPortrait({userId, file})
            .then((user)=> {
                this.fetchedUser(user);
                if (user && (typeof callback === 'function')) {
                    callback();
                }
            })
            .catch((error)=>console.log('Error when trying to upload portrait: ' + JSON.stringify(error)));
        return true;
    }
}