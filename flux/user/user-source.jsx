"use strict";
import {userRestClient} from "../../services/rest/rest-client.jsx";
import bamboostFlux from "../../bamboost-flux.jsx";

export default (alt) => ({
    fetchUser(id) {
        return {
            local(state,id){
                return state.users.getEntity(id);
            },
            remote(state, id) {
                return userRestClient.fetchUser(id);
            },
            success: alt.actions.userActions.fetchedUser,
            error: alt.actions.userActions.noUserFound
        }
    },
    fetchLoggedUser() {
        return {
            local(state,id){
                if (alt.bamboostContext.loggedUserId)
                    return state.users.getEntity(alt.bamboostContext.loggedUserId);
                else
                    return null;
            },
            remote(state) {
                if (alt.bamboostContext.loggedUserId)
                    return userRestClient.fetchUser(alt.bamboostContext.loggedUserId);
                else
                    return new Promise((resolve)=>resolve(null));
            },
            success: alt.actions.userActions.fetchedUser,
            error: alt.actions.userActions.noUserFound
        }
    },
    fetchUsersInOrganization(organizationId){
        return{
            remote(state, organizationId){
                return userRestClient.fetchUsersInOrganization(organizationId);
            },
            success: alt.actions.userActions.fetchedUsersInOrganization,
            error: alt.actions.userActions.noUserFound
        }
    }
});