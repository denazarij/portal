"use strict";
import {organizationRestClient} from "../../services/rest/rest-client.jsx";
import OrganizationActions from "./organization-actions.jsx";
import bamboostFlux from "../../bamboost-flux.jsx";

//noinspection JSUnusedLocalSymbols
export default (alt) => ({
    fetchOrganization(organizationId) {
        let organizationActions = alt.getActions("organizationActions");
        return {
            remote(state, organizationId) {
                return organizationRestClient.fetchOrganization(organizationId);
            },
            success: organizationActions.fetchedOrganization,
            error: organizationActions.organizationError
        }
    },

    createOrganization(organizationInput, callback) {
        let organizationActions = alt.getActions("organizationActions");
        return {
            remote(state, organizationInput) {
                return organizationRestClient.postOrganization(organizationInput);
            },
            interceptResponse(response, action, args){
                let callback = args[1];
                if (callback) callback(response);
                return response;
            },
            success: organizationActions.fetchedOrganization,
            error: organizationActions.organizationError
        }
    },

    deleteOrganization(organizationId, callback) {
        let organizationActions = alt.getActions("organizationActions");
        return {
            remote(state, organizationId) {
                return organizationRestClient.deleteOrganization(organizationId);
            },
            interceptResponse(response, action, args){
                let callback = args[1];
                if (callback) callback(organizationId);
                return response;
            },
            success: organizationActions.deletedOrganization,
            error: organizationActions.organizationError
        }
    },

    fetchSubOrganizations(id){
        return {
            remote(state, organizationId) {
                var offset = state.organizations['byParentId'].countEntities(organizationId);
                return organizationRestClient.fetchSubOrganizations(organizationId, offset);
            },
            success: alt.actions.organizationActions.fetchedSubOrganizations,
            error: alt.actions.organizationActions.organizationError
        }
    },

    searchSubOrganizations(id, organizationId, searchTerm, offset){
        return {
            remote(state, organizationId, searchTerm, offset) {
                if (offset == null || offset == 0) {
                    state.organizations["searchedByParentId"].resetEntities(organizationId);
                }
                return organizationRestClient.searchSubOrganizations(organizationId, searchTerm, offset);
            },
            success: alt.actions.organizationActions.searchedSubOrganizations,
            error: alt.actions.organizationActions.organizationError
        }
    },

    fetchUserOrganizations(userId){
        return {
            remote(state, userId){
                return organizationRestClient.fetchUserOrganizations(userId);
            },
            success: alt.actions.organizationActions.fetchedUserOrganizations,
            error: alt.actions.organizationActions.organizationError
        }
    },

    patch(patch) {
        let organizationActions = alt.getActions("organizationActions");
        return {
            remote(state, patch) {
                return organizationRestClient.patchOrganization(patch);
            },
            success: organizationActions.fetchedOrganization,
            error: organizationActions.organizationError
        }
    },

    uploadLogo(organizationId, file, callback){
        let organizationActions = alt.getActions("organizationActions");
        return{
            remote(state, organizationId, file){
                return organizationRestClient.uploadLogo(organizationId, file);
            },
            interceptResponse(response, action, args){
                let callback = args[2];
                if (callback) callback();
                return response;
            },
            success: organizationActions.fetchedOrganization,
            error: organizationActions.organizationError
        }

    },

    uploadMainImage(organizationId, file, callback){
        let organizationActions = alt.getActions("organizationActions");
        return{
            remote(state, organizationId, file){
                return organizationRestClient.uploadMainImage(organizationId, file);
            },
            interceptResponse(response, action, args){
                let callback = args[2];
                if (callback) callback();
                return response;
            },
            success: organizationActions.fetchedOrganization,
            error: organizationActions.organizationError
        }

    }

});