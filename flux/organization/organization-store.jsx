"use strict";

var _ = require("lodash");

import OrganizationSource from './organization-source.jsx';
import OrganizationActions from "./organization-actions.jsx";
import bamboostFlux from "../../bamboost-flux.jsx";

import GroupedEntities from "../grouped-entities.jsx";


//noinspection JSUnusedLocalSymbols
export default class OrganizationStore {
    constructor() {
        this.organizations = new GroupedEntities();
        this.organizations.addGroup("searchedByParentId", "parentId");
        this.organizations.addGroup("byParentId", "parentId", "id");
        this.organizations.addGroup("byUserId", "userIds");

        let organizationActions = this.alt.getActions("organizationActions");
        this.bindAction(organizationActions.deletedOrganization, this.onDeletedOrganization);
        this.bindAction(organizationActions.fetchedOrganization, this.onFetchedOrganization);
        this.bindAction(organizationActions.fetchedSubOrganizations, this.onFetchedSubOrganizations);
        this.bindAction(organizationActions.searchedSubOrganizations, this.onSearchedSubOrganizations);
        this.bindAction(organizationActions.fetchedUserOrganizations, this.onFetchedUserOrganizations);
        this.bindAction(organizationActions.organizationError, this.onOrganizationError);
        this.registerAsync(OrganizationSource);
    }

    onFetchedOrganization(organization) {
        this.organizations.addEntity(organization);
        this.organizations['byParentId'].addIfGrouped(organization);
    }

    onDeletedOrganization(organizationId){
        let organization = this.organizations.getEntity(organizationId);
        if (organization) this.organizations.remove(organization);
    }

    onFetchedSubOrganizations({organizationId, subOrganizations}) {
        subOrganizations.forEach((organization)=>this.onFetchedOrganization(organization));
        this.organizations['byParentId'].addEntitiesToGroup(subOrganizations, organizationId);
    }

    onFetchedUserOrganizations({userId, organizations}) {
        let userOrganizations = organizations.map((organization)=> {
            let currentOrganization = this.organizations.getEntity(organization.id);
            let userIds = [userId];
            if (currentOrganization && currentOrganization.userIds) {
                userIds = _.union(currentOrganization.userIds, userIds);
            }
            return _.extend({}, organization, {userIds});
        });
        userOrganizations.forEach((organization)=>this.onFetchedOrganization(organization));
        this.organizations['byUserId'].addEntitiesToGroup(userOrganizations, userId);
    }

    onSearchedSubOrganizations({organizationId, subOrganizations}) {
        subOrganizations.forEach((organization)=>this.onFetchedOrganization(organization));
        var searchResults = this.organizations['searchedByParentId'];
        searchResults.addEntitiesToGroup(subOrganizations, organizationId);
    }

    onOrganizationError(err) {
        console.error(err)
    }

}