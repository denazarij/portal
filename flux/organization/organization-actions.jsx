"use strict";

import {organizationRestClient} from "../../services/rest/rest-client.jsx";

//noinspection JSUnusedLocalSymbols
export default class OrganizationActions {

    //noinspection JSMethodCanBeStatic
    fetchedOrganization(organization) { return organization; }

    //noinspection JSMethodCanBeStatic
    deletedOrganization(organizationId) { return organizationId; }

    //noinspection JSMethodCanBeStatic
    fetchedSubOrganizations({organizationId, subOrganizations}) {
        return {organizationId, subOrganizations};
    }

    //noinspection JSMethodCanBeStatic
    fetchedUserOrganizations({userId, organizations}) { return {userId, organizations}; }

    //noinspection JSMethodCanBeStatic
    searchedSubOrganizations({organizationId, subOrganizations}) {return {organizationId, subOrganizations};}

    organizationError(error) {return error}

}