"use strict";

var React = require("react");
var AltContainer = require("alt-container");

import {Link} from 'react-router';
import Spinner from '../spinner/spinner.jsx';

let DashboardPropTypes = {
    route: React.PropTypes.object.isRequired,
    params: React.PropTypes.object.isRequired
};
let DashboardDefaultProps = {};
export default class Dashboard extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let organizationStore = this.props.route.flux.getStore("organizationStore");
        let userStore = this.props.route.flux.getStore("userStore");

        let inject = {
            organizations: ()=> {
                let user = userStore.getLoggedUser();
                if (user) {
                    var organizationsByUserId = organizationStore.getState().organizations["byUserId"];
                    if (organizationsByUserId.isGrouped(user.id))
                        return organizationsByUserId.getEntities(user.id);
                    else {
                        organizationStore.fetchUserOrganizations(user.id);
                    }
                }
                return [];
            }
        };
        return (
            <AltContainer stores={[organizationStore, userStore]} inject={inject}>
                <OrganizationList />
            </AltContainer>
        );
    }
};
Dashboard.propTypes = DashboardPropTypes;
Dashboard.defaultProps = DashboardDefaultProps;

var OrganizationList = ({organizations}) => {
    if (!organizations || organizations.length == 0)
        return <Spinner />;
    return (
        <ul>
            {organizations.map((organization)=> {
                let path = organization.parentId
                    ? "/projects/" + organization.id
                    : "/communities/" + organization.id;
                return (
                    <li key={organization.id}>
                        <Link to={path}>{organization.name}</Link>
                    </li>
                )
            })}
        </ul>
    )
};