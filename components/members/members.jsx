"use strict";

var React = require('react');
var AltContainer = require('alt-container');
import UserDisplay from "../user/user-display.jsx";
import {getTranslation as i18n} from "../../services/translations/translations.jsx";
import SearchField from "../form/search-bar.jsx";

let MembersPropTypes = {
    flux: React.PropTypes.object.isRequired,
    organizationId: React.PropTypes.number.isRequired
};
let MembersDefaultProps = {};
export default class Members extends React.Component {
    constructor(props) {
        super(props);
    }

    doSearch(searchTerm){
        console.error("Not implemented");
    }

    render() {
        let userStore = this.props.flux.getStore('userStore');
        let organizationStore = this.props.flux.getStore('organizationStore');
        if (!userStore.getState().users['byOrganizationId'].isGrouped(this.props.organizationId))
            userStore.fetchUsersInOrganization(this.props.organizationId);

        let inject = {
            users: ()=> {
                let group = userStore.getState().users['byOrganizationId'];
                return group.getEntities(this.props.organizationId);
            },
            organization: ()=>{
                return organizationStore.getState().organizations.getEntity(this.props.organizationId)
            }
        };
        return (
            <AltContainer stores={[userStore, organizationStore]} inject={inject}>
                <SearchField className="right" onSearch={this.doSearch.bind(this)}/>
                <h1>{i18n("section.members")}</h1>
                <UserDisplay/>
            </AltContainer>
        );
    }
};
Members.propTypes = MembersPropTypes;
Members.defaultProps = MembersDefaultProps;
