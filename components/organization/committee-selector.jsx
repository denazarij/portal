"use strict";

var React = require("react");

import {Row, Column} from "../grid/grid.jsx";
import {getTranslation as i18n} from "../../services/translations/translations.jsx";
import Spinner from "../spinner/spinner.jsx";
import Error from "../form/error.jsx";
import Checkbox from "../form/checkbox.jsx"

export class UncheckedCommitteeSelector extends React.Component {
    constructor(props) {
        super(props);
    }

    changeSelection(committee, selected) {
        if (selected) {
            let newSelection = _.union(this.props.selectedCommittees, [committee]);
            this.props.onChangeSelection(newSelection);
        } else {
            let newSelection = _.reject(this.props.selectedCommittees,
                (currentCommittee)=>currentCommittee.id === committee.id);
            this.props.onChangeSelection(newSelection);
        }
    }

    render() {
        let selectedCommitteeIds = this.props.selectedCommittees.map((committee)=>committee.id);
        let disabledCommitteesIds = this.props.disabledCommittees.map((committee)=>committee.id);

        return (
            <span>
                {this.props.committees.map((committee)=> {
                    let selected = _.contains(selectedCommitteeIds, committee.id);
                    let disabled = _.contains(disabledCommitteesIds, committee.id);

                    if (disabled) {
                        return (<span className="disabled-committee">
                                    <i className="fa fa-check-square-o"/>
                                    <span className="committee-name">{committee.name}</span>
                                </span>);
                    } else {
                        return (
                            <Checkbox
                                key={committee.id}
                                label={committee.name}
                                checked={selected}
                                onChange={()=>this.changeSelection(committee, !selected)} />
                        )
                    }
                })}
            </span>
        )
    }
}

UncheckedCommitteeSelector.propTypes = {
    committees: React.PropTypes.array.isRequired,
    onChangeSelection: React.PropTypes.func.isRequired,
    selectedCommittees: React.PropTypes.array,
    disabledCommittees: React.PropTypes.array
};

UncheckedCommitteeSelector.defaultProps = {
    selectedCommittees: [],
    disabledCommittees: []
};


export default class CommitteeSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            communityRoleNotSelected: false,
            disableSelected: React.PropTypes.bool
        }
    }

    validate() {
        let communityRoleSelected = this.refs.uncheckedCommitteeSelector.validate();
        this.setState({communityRoleNotSelected: !communityRoleSelected});
        return communityRoleSelected;
    }

    render() {
        let {...uncheckedProps} = this.props;
        return (
            <div>
                <Row>
                    <Column width={12}>
                        {i18n('member.add.roles.in.community', {communityName: this.props.organizationName})}
                    </Column>
                </Row>
                <Error hasError={this.state.communityRoleNotSelected}
                    errorMessage={i18n('member.add.community.role.not.selected')}/>
                <Row className="community-roles-wrapper">
                    <Column width={12}>
                        <UncheckedCommitteeSelector ref="uncheckedCommitteeSelector" {...uncheckedProps} />
                    </Column>
                </Row>
            </div>
        );
    }
};

CommitteeSelector.propTypes = {
    committees: React.PropTypes.array.isRequired,
    onChangeSelection: React.PropTypes.func.isRequired,
    selectedCommittees: React.PropTypes.array,
    errors: React.PropTypes.object,
    disabledCommittees: React.PropTypes.array
};

CommitteeSelector.defaultProps = {
    selectedCommittees: [],
    errors: {},
    disabledCommittees: []
};