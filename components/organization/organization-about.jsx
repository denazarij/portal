"use strict";

require("./organization.scss");

import React from "react";
import AltContainer from 'alt-container';

import OrganizationHeader from "./organization-header.jsx";

const propTypes = {
    route: React.PropTypes.object.isRequired,
    params: React.PropTypes.object.isRequired
};

const defaultProps = {};

export default class OrganizationAbout extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let organizationStore = this.props.route.flux.getStore("organizationStore");
        let organizationId = parseInt(this.props.params.organizationId);
        setTimeout(()=> {
            organizationStore.fetchOrganization(organizationId);
        })
    }

    render() {
        let organizationStore = this.props.route.flux.getStore("organizationStore");
        let organizationId = parseInt(this.props.params.organizationId);
        let inject = {
            organization: ()=>organizationStore.getState().organizations.getEntity(organizationId)
        };
        return (
            <AltContainer stores={[organizationStore]} inject={inject}>
                <OrganizationHeader flux={this.props.route.flux} />
            </AltContainer>
        );
    }
}

OrganizationAbout.propTypes = propTypes;
OrganizationAbout.defaultProps = defaultProps;