"use strict";

require("./organization.scss");

import React from "react";
import { Link } from 'react-router';

const propTypes = {
    initialSomeValue: React.PropTypes.string
};

const defaultProps = {};

export default class OrganizationMenu extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            someValue: this.props.initialSomeValue
        }
    }
    
    //noinspection JSMethodCanBeStatic
    render() {
        return (
            <ul>
                <li><Link to="/organization/1/messages">Messages</Link></li>
                <li><Link to="/organization/1/books">Books</Link></li>
                <li><Link to="/organization/1/bookStructures">BookStructures</Link></li>
                <li><Link to="/organization/1/members">Members</Link></li>
                <li><Link to="/organization/1/about">About</Link></li>
            </ul>
        );
    }
}

OrganizationMenu.propTypes = propTypes;
OrganizationMenu.defaultProps = defaultProps;
