"use strict";

require("./organization.scss");

var _ = require('lodash');
var cx = require('classnames');
import React from "react";
import AltContainer from 'alt-container';

import Spinner from "../spinner/spinner.jsx";
import {getTranslation as i18n} from "../../services/translations/translations.jsx";
import {Row, Column} from "../grid/grid.jsx";
import EditableLabel from "../form/editable/editable-label.jsx";
import EditableHtml from "../form/editable/editable-html.jsx";
import IconButton from "../form/icon-button.jsx";
import OrganizationAdminLinks from "./organization-admin-links.jsx";

const propTypes = {
    flux: React.PropTypes.object.isRequired,
    organization: React.PropTypes.object,
    pageHolder: React.PropTypes.object,
    editable: React.PropTypes.bool
};
const defaultProps = {
    editable: true
};

export default class OrganizationHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isEditingNow: false
        };
    }

    render() {
        let organization = this.props.organization;
        let organizationStore = this.props.flux.getStore('organizationStore');
        let inject = {
            organization: ()=>organizationStore.getState().organizations.getEntity(organization.id)
        };
        if (!organization)
            return false;
        if (!organization)
            return <Spinner text={i18n('organization.about.loading')}/>;
        else {
            let allowEdit = _.includes(organization.actionsAllowed, "UPDATE");
            return (
                <div>
                    <div className={cx("organization-description", {"is-editing": this.state.isEditingNow})}>
                        {organization.mainImageUrl && (
                            <img className="main-image" id="imageName" src={organization.mainImageUrl}/>
                        )}
                        <EditableHtml allowEdit={allowEdit && !this.state.isEditingNow}
                                      onToggleEditing={isEditingNow => this.setState({isEditingNow})}
                                      onNewValue={(value)=>organizationStore.patch({id:organization.id, description:value})}>
                            {organization.description}
                        </EditableHtml>
                    </div>
                    {allowEdit && (
                        <AltContainer stores={[organizationStore]} inject={inject}>
                            <OrganizationAdminLinks flux={this.props.flux}/>
                        </AltContainer>
                    )}
                </div>
            );
        }
    }
}

OrganizationHeader.propTypes = propTypes;
OrganizationHeader.defaultProps = defaultProps;
