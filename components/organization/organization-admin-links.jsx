"use strict";

require("./organization.scss");

import React from "react";

import Spinner from "../spinner/spinner.jsx";
import {getTranslation as i18n} from "../../services/translations/translations.jsx";
import {Row, Column} from "../grid/grid.jsx";
import Upload from "../form/upload.jsx";
import IconButton from "../form/icon-button.jsx";

const propTypes = {
    editDisabled: React.PropTypes.bool
};
const defaultProps = {};

export default class OrganizationAdminLinks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            uploadingLogo: false,
            uploadingMainImage: false
        };
    }

    handleUploadLogo(file) {
        var organizationStore = this.props.flux.getStore("organizationStore");
        organizationStore.uploadLogo(this.props.organization.id, file,
            ()=>this.setState({uploadingLogo: false}));
        this.setState({uploadingLogo: true})
    }

    handleUploadMainImage(file) {
        var organizationStore = this.props.flux.getStore("organizationStore");
        organizationStore.uploadMainImage(this.props.organization.id, file,
            ()=>this.setState({uploadingMainImage: false}));
        this.setState({uploadingMainImage: true})
    }

    render() {
        return (
            !this.props.editDisabled && (
                <div className="clearfix">
                    <div className="admin-controls">
                        <IconButton iconName="gear" className="group-icon"/>
                        <Upload onUpload={this.handleUploadMainImage.bind(this)}>
                            <span>
                                {this.state.uploadingMainImage
                                    ? <Spinner />
                                    : <IconButton iconName="upload">
                                    {i18n('organization.upload-main-image')}
                                </IconButton>
                                }
                            </span>
                        </Upload>
                        <Upload onUpload={this.handleUploadLogo.bind(this)}>
                            <span>
                                {this.state.uploadingLogo
                                    ? <Spinner />
                                    : <IconButton iconName="upload">
                                    {i18n('organization.upload-logo')}
                                </IconButton>
                                }
                            </span>
                        </Upload>
                        <IconButton iconName="upload">Upload sponsor image</IconButton>
                    </div>
                </div>
            )
        )
    }
}

OrganizationAdminLinks.propTypes = propTypes;
OrganizationAdminLinks.defaultProps = defaultProps;

