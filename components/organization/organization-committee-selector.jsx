"use strict";

var _ = require("lodash");
var React = require("react");
var AltContainer = require('alt-container');

import {UncheckedCommitteeSelector} from './committee-selector.jsx';

let AudienceSelector = ({committees, selectedCommittees, onChangeSelection})=>{
    return committees &&
    <UncheckedCommitteeSelector
        className="audience-selector"
        committees={committees}
        selectedCommittees = {selectedCommittees || _.cloneDeep(committees)}
        onChangeSelection = {onChangeSelection}/>
};

let OrganizationCommitteeSelectorPropTypes = {
    flux: React.PropTypes.object.isRequired,
    organizationId: React.PropTypes.number.isRequired
};
let OrganizationCommitteeSelectorDefaultProps = {};
export default class OrganizationCommitteeSelector extends React.Component {
    constructor(props) {
        super(props);
        this.state = {selection:null}
    }

    //noinspection JSUnusedGlobalSymbols
    componentDidMount(){
        let organizationStore = this.props.flux.getStore('organizationStore');
        if (!organizationStore.getState().organizations.getEntity(this.props.organizationId))
            organizationStore.fetchOrganization(this.props.organizationId);
    }

    getCommitteeIds(){
        return this.state.selection.map((committee)=>committee.id);
    }

    render() {
        let organizationStore = this.props.flux.getStore('organizationStore');
        let inject = {
            committees: ()=>{
                let organization = organizationStore.getState().organizations.getEntity(this.props.organizationId);
                return organization ? organization.committees : [];
            }
        };
        return (
            <AltContainer stores={[organizationStore]} inject={inject}>
                <AudienceSelector
                    committees={[]}
                    selectedCommittees = {this.state.selection}
                    onChangeSelection = {(selection)=>this.setState({selection})}/>
            </AltContainer>
        );
    }
};
OrganizationCommitteeSelector.propTypes = OrganizationCommitteeSelectorPropTypes;
OrganizationCommitteeSelector.defaultProps = OrganizationCommitteeSelectorDefaultProps;