"use strict";

require("./../organization.scss");

var Link = require('react-router/lib/Link');

import {getTranslation as i18n} from "../../../services/translations/translations.jsx";
import FormattedHtml from "../../formatted-html/formatted-html.jsx";
import ImageCell from "../../image-cell/image-cell.jsx";
import Spinner from "../../spinner/spinner.jsx";

var Portfolio = ({show, organization, subOrganizations, onFetchMore}) => {
    if (!show)
        return <div/>;
    else if (!organization)
        return <Spinner text={i18n('organization.about.loading')}/>;
    else {
        return (
            <div className="project-list">
                <ul>
                    {subOrganizations.map((organization)=> {
                        let path = organization.parentId
                            ? "/projects/" + organization.id
                            : "/communities/" + organization.id;
                        return (
                            <ImageCell imageUrl={organization.mainImageUrl}
                                      titleText={organization.name}
                                      additionalText={organization.description}
                                      path={path}
                                      key={organization.id}/>

                        )
                    })}
                </ul>
                <button onClick={onFetchMore}>{i18n('projects.more')}</button>
            </div>
        );
    }
};

export default Portfolio;