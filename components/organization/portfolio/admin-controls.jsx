"use strict";

var React = require('react');
var _ = require('lodash');
import {getTranslation as i18n} from "../../../services/translations/translations.jsx";
import IconButton from "../../form/icon-button.jsx";

let PortfolioAdminControlsPropTypes = {
    setView: React.PropTypes.func.isRequired,
    organization: React.PropTypes.object,
    history: React.PropTypes.object
};
let PortfolioAdminControlsDefaultProps = {};
export default class PortfolioAdminControls extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {organization, history} = this.props;
        if (!organization || !_.includes(organization.actionsAllowed, "UPDATE"))
            return false;
        let addCommunityButtons = !organization.parentId;
        return (
            <div className="clearfix">
                <div className="admin-controls ">
                    <IconButton iconName="gear" className="group-icon"/>
                    {addCommunityButtons && (
                        <IconButton iconName="plus"
                                    onClick={()=>this.props.setView("new-project")}>
                            {i18n('portfolio.add-project')}
                        </IconButton>
                    )}
                    <IconButton iconName="pencil"
                                onClick={()=>history.push("/communities/" + organization.id + "/methodology")}>
                        {i18n('community.edit-methodology')}
                    </IconButton>
                </div>
            </div>
        )
    }
};
PortfolioAdminControls.propTypes = PortfolioAdminControlsPropTypes;
PortfolioAdminControls.defaultProps = PortfolioAdminControlsDefaultProps;