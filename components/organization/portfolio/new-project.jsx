"use strict";

var React = require("react");

import Input from "../../form/input.jsx";
import IconButton from "../../form/icon-button.jsx";
import {getTranslation as i18n} from "../../../services/translations/translations.jsx";


let NewProjectPropTypes = {
    show: React.PropTypes.bool.isRequired, // FIXME remove this when AltContainer is fixed to allow false
    flux: React.PropTypes.object.isRequired,
    onCancel: React.PropTypes.func.isRequired,
    organization: React.PropTypes.object
};
let NewProjectDefaultProps = {};
export default class NewProject extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            isPending: false,
            name:""
        }
    }

    createProject(){
        if (!this.state.name){
            console.log("Name required");
            return;
            //FIXME add error display
        }
        let organizationStore = this.props.flux.getStore("organizationStore");
        var organizationInput = {
            parentId: this.props.organization.id,
            name: this.state.name
        };
        organizationStore.createOrganization(
            organizationInput,
            (project)=>setTimeout(()=>this.props.history.push("/projects/" + project.id))
        )
    }

    render() {
        if (!this.props.show || !this.props.organization) return false;
        return (
            <div className="stacked-form in-page-form">
                <div className="title">{i18n("project.new-project-title")}</div>
                <Input label={i18n("project.name")}
                       value={this.state.name}
                       onChange={event=>this.setState({name: event.target.value})} />
                <div className="button-row">
                    <IconButton iconName="save" onClick={this.createProject.bind(this)}>
                        {i18n("button.save")}
                    </IconButton>
                    <IconButton iconName="times" onClick={this.props.onCancel}>
                        {i18n("button.cancel")}
                    </IconButton>
                </div>
            </div>
        );
    }
}
NewProject.propTypes = NewProjectPropTypes;
NewProject.defaultProps = NewProjectDefaultProps;
