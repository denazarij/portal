"use strict";

require("./../organization.scss");

var React = require('react');
var AltContainer = require('alt-container');

import Spinner from "../../spinner/spinner.jsx";
import {getTranslation as i18n} from "../../../services/translations/translations.jsx";
import {Row, Column} from "../../grid/grid.jsx";
import IconButton from "../../form/icon-button.jsx";
import SearchField from "../../form/search-bar.jsx";

import Portfolio from './portfolio.jsx';
import PortfolioAdminControls from './admin-controls.jsx';
import NewProject from './new-project.jsx';


const propTypes = {
    route: React.PropTypes.object.isRequired,
    params: React.PropTypes.object.isRequired,
    history: React.PropTypes.object.isRequired
};

const defaultProps = {};

export default class OrganizationPortfolio extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchTerm: "",
            view: ""
        }
    }

    componentDidMount() {
        var props = this.props;
        OrganizationPortfolio.fetchIfNeeded(props);
    }

    componentWillReceiveProps(newProps) {
        OrganizationPortfolio.fetchIfNeeded(newProps);
    }

    static fetchIfNeeded(props) {
        let organizationStore = props.route.flux.getStore("organizationStore");
        let organizationId = parseInt(props.params.organizationId);
        let organizations = organizationStore.getState().organizations;
        if (organizations.getEntity(organizationId) == null)
            organizationStore.fetchOrganization(organizationId);
        if (!organizations['byParentId'].isGrouped(organizationId))
            organizationStore.fetchSubOrganizations(organizationId);
    }

    fetchMore() {
        let organizationStore = this.props.route.flux.getStore("organizationStore");
        let organizationId = parseInt(this.props.params.organizationId);
        if (this.state.searchTerm == "") {
            organizationStore.fetchSubOrganizations(organizationId);
        } else {
            let searchedGroup = organizationStore.getState().organizations["searchedByParentId"];
            let offset = searchedGroup.countEntities(organizationId);
            organizationStore.searchSubOrganizations(organizationId, this.state.searchTerm, offset);
        }
    }

    doSearch(searchTerm) {
        let organizationStore = this.props.route.flux.getStore("organizationStore");
        let organizationId = parseInt(this.props.params.organizationId);
        this.setState({searchTerm: searchTerm});
        if (searchTerm) {
            organizationStore.searchSubOrganizations(organizationId, searchTerm);
        }
    }

    render() {
        let organizationStore = this.props.route.flux.getStore("organizationStore");
        let organizationId = parseInt(this.props.params.organizationId);
        let inject = {
            organization: ()=>organizationStore.getState().organizations.getEntity(organizationId),
            subOrganizations: getSubOrganizations.bind(this)
        };
        return (
            <div>
                <AltContainer stores={[organizationStore]} inject={inject}>
                    <SearchField className="right" onSearch={this.doSearch.bind(this)}/>
                    <h1>{i18n("section.projects")}</h1>
                    <NewProject show={this.state.view === "new-project"}
                                flux={this.props.route.flux}
                                history={this.props.history}
                                onCancel={()=>this.setState({view:""})}/>
                    <Portfolio show={this.state.view === ""}
                               onFetchMore={this.fetchMore.bind(this)}/>
                    <PortfolioAdminControls history={this.props.history}
                                            setView={view => this.setState({view})}/>
                </AltContainer>
            </div>
        );

        function getSubOrganizations() {
            if (this.state.searchTerm) {
                return organizationStore.getState().organizations['searchedByParentId'].getEntities(organizationId);
            } else {
                return organizationStore.getState().organizations['byParentId'].getEntities(organizationId);
            }
        }
    }
}

OrganizationPortfolio.propTypes = propTypes;
OrganizationPortfolio.defaultProps = defaultProps;

var Section = ({children,...props})=> (
    <section>
        {React.Children.map(children, (child)=> {
            React.cloneElement(child, ...props);
        })}
    </section>
)