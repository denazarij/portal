"use strict";

require("./organization.scss");

var _ = require('lodash');
import React from "react";
import AltContainer from 'alt-container';

import Spinner from "../spinner/spinner.jsx";
import {getTranslation as i18n} from "../../services/translations/translations.jsx";
import {Row, Column} from "../grid/grid.jsx";
import IconButton from "../form/icon-button.jsx";
import OrganizationAdminLinks from "./organization-admin-links.jsx";
import EditableLabel from "../form/editable/editable-label.jsx";

const propTypes = {
    flux: React.PropTypes.object.isRequired,
    organization: React.PropTypes.object,
    editable: React.PropTypes.bool
};
const defaultProps = {
    editable: true
};

export default class OrganizationTitle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isEditingNow: false
        };
    }

    render() {
        let organization = this.props.organization;
        let organizationStore = this.props.flux.getStore('organizationStore');
        if (!organization)
            return false;
        if (!organization)
            return <Spinner text={i18n('organization.about.loading')}/>;
        else {
            let allowEdit = _.includes(organization.actionsAllowed, "UPDATE");
            return (
                <span>
                    {organization.logoUrl ? <img className="header-logo" src={organization.logoUrl} /> : <span></span>}
                    <EditableLabel allowEdit={allowEdit && !this.state.isEditingNow}
                        className="inline-block organization-name"
                        onToggleEditing={(isEditingNow)=>this.setState({isEditingNow})}
                        onNewValue={(value)=>organizationStore.patch({id:organization.id, name:value})}>
                        {organization.name}
                    </EditableLabel>
                </span>)
        }
    }
}

OrganizationTitle.propTypes = propTypes;
OrganizationTitle.defaultProps = defaultProps;
