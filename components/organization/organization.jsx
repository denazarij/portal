"use strict";

require("./organization.scss");

import React from "react";
import {Link} from 'react-router';

const propTypes = {
    user: React.PropTypes.object
};

const defaultProps = {};

export default class Organization extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            someValue: this.props.initialSomeValue
        }
    }
    
    render() {
        let {menu, content} = this.props;
        return (
            <div className="page">
                <div className="menu">
                    {menu}
                </div>
                <div className="content">
                    {content}
                </div>
            </div>
        )
    }
}

Organization.propTypes = propTypes;
Organization.defaultProps = defaultProps;
