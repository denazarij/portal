"use strict";

require("./organization.scss");

var React = require("react");
var AltContainer = require("alt-container");

import IconButton from "../form/icon-button.jsx";
import Input from "../form/input.jsx";
import {getTranslation as i18n} from "../../services/translations/translations.jsx";

const propTypes = {
    route: React.PropTypes.object.isRequired,
    params: React.PropTypes.object.isRequired,
    history: React.PropTypes.object.isRequired
};

const defaultProps = {};

export default class OrganizationConfiguration extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let organizationStore = this.props.route.flux.getStore("organizationStore");
        let organizationId = parseInt(this.props.params.organizationId);
        setTimeout(()=> {
            organizationStore.fetchOrganization(organizationId);
        })
    }

    render() {
        let organizationStore = this.props.route.flux.getStore("organizationStore");
        let organizationId = parseInt(this.props.params.organizationId);
        let inject = {
            organization: ()=>organizationStore.getState().organizations.getEntity(organizationId)
        };
        return (
            <AltContainer stores={[organizationStore]} inject={inject}>
                <DeleteOrganization flux={this.props.route.flux}
                                    history={this.props.history}/>
            </AltContainer>
        );
    }
}
OrganizationConfiguration.propTypes = propTypes;
OrganizationConfiguration.defaultProps = defaultProps;


let DeleteOrganizationPropTypes = {
    organization: React.PropTypes.object,
    flux: React.PropTypes.object.isRequired,
    history: React.PropTypes.object.isRequired
};
let DeleteOrganizationDefaultProps = {};
class DeleteOrganization extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            error: ""
        }
    }

    deleteOrganization() {
        if (this.state.name !== this.props.organization.name) {
            this.setState({error: i18n("error.mismatch")})
        } else {
            let organizationStore = this.props.flux.getStore("organizationStore");
            var redirect = ()=>this.props.history.push("/communities/" + this.props.organization.parentId);
            organizationStore.deleteOrganization(this.props.organization.id, redirect)
        }
    }

    render() {
        if (!this.props.organization)
            return false;
        return (
            <div className="stacked-form">
                <Input label={i18n("organization.name")}
                       placeholder={i18n("organization.enter.name.to.delete")}
                       value={this.state.name}
                       onChange={event=>this.setState({error:"", name:event.target.value})}>
                </Input>
                {this.state.error && (
                    <div className="error">{this.state.error}</div>
                )}
                <IconButton iconName="trash"
                            onClick={this.deleteOrganization.bind(this)}>
                    {i18n("button.delete")}
                </IconButton>
            </div>
        );
    }
};
DeleteOrganization.propTypes = DeleteOrganizationPropTypes;
DeleteOrganization.defaultProps = DeleteOrganizationDefaultProps;