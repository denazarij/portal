"use strict";

require("./grid.scss");

var React = require("react");
var classNames = require("classnames");

export class Row extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={classNames("row", this.props.className)}>
                {this.props.children}
            </div>
        );
    }
}
Row.propTypes = {
    className: React.PropTypes.string
};

export class FlexRow extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={classNames("row flex-row", this.props.className)}>
                {this.props.children}
            </div>
        );
    }
}
Row.propTypes = {
    className: React.PropTypes.string
};

export class Column extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let columnClasses;
        if (this.props.width) {
            columnClasses = classNames("column", "column-" + this.props.width, this.props.className);
        } else {
            columnClasses = classNames("fixed-col", this.props.className);
        }

        return (
            <div className={columnClasses}>
                {this.props.children}
            </div>
        );
    }
}
Column.propTypes = {
    className: React.PropTypes.string,
    width: React.PropTypes.number
};
Column.defaultProps = {
    width: 12
};


export class FlexColumn extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={classNames("flex-col", this.props.className)}>
                {this.props.children}
            </div>
        );
    }
}
FlexColumn.propTypes = {
    className: React.PropTypes.string
};