"use strict";

require("./filter.scss");

var React = require("react");

import {Row, Column, FlexRow, FlexColumn} from "../grid/grid.jsx";

export default class Filter extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expanded: this.props.initialExpandFilter
        }
    }

    expandCollapseFilter() {
        this.setState({'expanded': !this.state.expanded});
    }

    render() {
        let arrowClassName = this.state.expanded ? "fa fa-caret-left filter-arrow-icon" : "fa fa-caret-down filter-arrow-icon";
        let showClassName = this.state.expanded ? "filter-content" : "hide";

        return (
            <div>
                <FlexRow>
                    <Column className="filter-header">
                        <div onClick={this.expandCollapseFilter.bind(this)}>
                            <i className="fa fa-filter filter-icon"/>

                            <div className="filter-value">{this.props.value}</div>
                            <i className={arrowClassName}/>
                        </div>
                    </Column>
                    <FlexColumn className="none-filter-header">
                        <hr />
                    </FlexColumn>
                </FlexRow>
                <Row className={showClassName}>
                    <Column width={12}>
                        {this.props.children}
                    </Column>
                </Row>
            </div>
        );
    }
}

Filter.propTypes = {
    initialExpandFilter: React.PropTypes.bool
};

Filter.defaultProps = {
    initialExpandFilter: false
};