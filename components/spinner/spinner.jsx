"use strict";

require('./spinner.scss');

var React = require("react");

export default class Spinner extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let showHideClassName = this.props.show ? 'spinner-wrapper' : 'hide';

        return (
            <span className={showHideClassName}>
                <i className="fa fa-spinner fa-spin spinner"/>
                {this.props.text.length ? <span className="spinner-text">{this.props.text}</span> : ''}
            </span>
        );
    }
}

Spinner.propTypes = {
    show: React.PropTypes.bool.isRequired,
    text: React.PropTypes.string
};

Spinner.defaultProps = {
    show: true,
    text: ''
};