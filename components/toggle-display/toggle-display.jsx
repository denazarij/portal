"use strict";

require("./toggle-display.scss");

var React = require("react");
var classNames = require('classnames');

/*
 ToggleDisplay component usage:
 <ToggleDisplay initialShowContent="true" onShowContent="function">
 ...
 <ToggleDisplayTrigger>...</ToggleDisplayTrigger>
 <ToggleDisplayContent>...</ToggleDisplayContent>
 ...
 </ToggleDisplay>
 */

export default class ToggleDisplay extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showContent: this.props.initialShowContent
        }
    }

    toggleHandler() {
        this.setState({showContent: !this.state.showContent});
        this.props.onShowContent && this.props.onShowContent(this.state.showContent);
    }

    validateComponentStructure() {
        let toggleDisplayTriggerPresent = false;
        let toggleDisplayContentPresent = false;

        React.Children.forEach(this.props.children, (child) => {
            if (child.type == ToggleDisplayTrigger) {
                toggleDisplayTriggerPresent = true;
            } else if (child.type == ToggleDisplayContent) {
                toggleDisplayContentPresent = true;
            }
        });

        if (!toggleDisplayTriggerPresent) {
            if (console)
                console.log("Warning: <ToggleDisplayTrigger /> missing in <ToggleDisplay /> component");
        }
        if (!toggleDisplayContentPresent) {
            if (console)
                console.log("Warning: <ToggleDisplayContent /> missing in <ToggleDisplay /> component");
        }
    }

    renderChildren() {
        if (process.env.NODE_ENV !== "production") {
            this.validateComponentStructure();
        }

        return React.Children.map(this.props.children,
            (child) => {
                switch(child.type){
                    case ToggleDisplayTrigger:
                        return (
                            <div onClick={this.toggleHandler.bind(this)}>
                                {React.cloneElement(child, {isOn: this.state.showContent})}
                            </div>
                        );
                    case ToggleDisplayContent:
                        return this.state.showContent && child;
                    case ToggleDisplayDefault:
                        return !this.state.showContent && child;
                    default:
                        return child;
                }
            }
        );
    }

    render() {
        return (
            <div className={classNames("toggle-display-container", this.props.className)}>
                {this.renderChildren()}
            </div>
        );
    }
}

ToggleDisplay.propTypes = {
    initialShowContent: React.PropTypes.bool,
    onShowContent: React.PropTypes.func
};

ToggleDisplay.defaultProps = {
    initialShowContent: false
};

var ToggleDisplayTrigger = ({className, children, isOn}) => (
    <div className={classNames("toggle-display-trigger", className)}>
        {React.Children.map(children, (child)=>(
            React.cloneElement(child, {isOn: !!isOn})
        ))}
    </div>
);

var ToggleDisplayContent = ({className, children})=>(
    <div className={classNames("toggle-display-content", className)}>{children}</div>
);

var ToggleDisplayDefault = ({className, children})=>(
    <div className={classNames("toggle-default-content", className)}>{children}</div>
);

ToggleDisplay.Trigger = ToggleDisplayTrigger;
ToggleDisplay.Content = ToggleDisplayContent;
ToggleDisplay.Default = ToggleDisplayDefault;