"use strict";

require("./tabs.scss");

var React = require("react");

import Tab from "./tab.jsx";
import TabTemplate from "./tab-template.jsx";
import {Row, Column} from "../grid/grid.jsx";

export default class Tabs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: this.props.selectedIndex ? this.props.selectedIndex : this.props.initialSelectedIndex
        }
    }

    handleTabChange(tabIndex) {
        this.setState({selectedIndex: tabIndex});
        if (this.props.tabChangeHandler != undefined) {
            this.props.tabChangeHandler(tabIndex);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selectedIndex != undefined) {
            this.setState({selectedIndex: nextProps.selectedIndex});
        }
    }

    render() {
        let tabsContent = false;
        let tabs = React.Children.map(this.props.children, (tab, index) => {
            if (tab.type == Tab) {
                if (this.state.selectedIndex === index) {
                    tabsContent = <div>{tab.props.children}</div>
                }
                return React.cloneElement(tab, {
                    key: 'tab-' + index,
                    tabIndex: index,
                    selected: this.state.selectedIndex === index,
                    handleTabChange: this.handleTabChange.bind(this)
                });
            } else {
                if (process.env.NODE_ENV !== "production") {
                    let type = tab.type;
                    throw 'Tabs only accepts Tab Components as children. Found ' +
                    type + ' as child number ' + (index + 1) + ' of Tabs';
                }
            }
        }, this);
        let tabsWidth = this.props.width || 2;
        let tabProps = {tabs,tabsContent,tabsWidth};
        return this.props.vertical
            ? <VerticalTabs {...tabProps} />
            : <HorizontalTabs {...tabProps} />;
    }
}

Tabs.propTypes = {
    initialSelectedIndex: React.PropTypes.number,
    selectedIndex: React.PropTypes.number,
    vertical: React.PropTypes.bool,
    tabChangeHandler: React.PropTypes.func
};

Tabs.defaultProps = {
    initialSelectedIndex: 0,
    vertical: false
};

var VerticalTabs = ({tabs, tabsContent, tabsWidth})=>(
    <Row className="vertical-tab">
        <Column width={tabsWidth} className="tabs-wrapper" key="tab">
            {tabs}
        </Column>
        <Column width={12-tabsWidth} key="tab-content">
            {tabsContent}
        </Column>
    </Row>
);

var HorizontalTabs = ({tabs, tabsContent})=>(
    <div className="horizontal-tab">
        <Row>
            <Column width={12} className="tabs-wrapper" key="tab">
                {tabs}
            </Column>
        </Row>
        <Row className="tab-content-wrapper">
            <Column width={12} className="tab-content" key="tab-content">
                {tabsContent}
            </Column>
        </Row>
    </div>
);