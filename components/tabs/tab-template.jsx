"use strict";

var React = require("react");

export default class TabTemplate extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let children = React.Children.map(this.props.children, (child) => {
            return React.cloneElement(child, {selectedTab: this.props.selected});
        }, this);
        return (
            this.props.selected && (
                <div>{children}</div>
            )
        );
    }
}
