"use strict";

var React = require("react");
var classNames = require('classnames');

export default class Tab extends React.Component {
    constructor(props) {
        super(props);
    }

    handleTabChange() {
        this.props.handleTabChange(this.props.tabIndex);
    }

    render() {
        let selectedTabClassName = this.props.selected ? "selected" : "";
        let notificationsClassName = this.props.notifications ? "notifications" : "hide";

        return (
                <div className={classNames("tab", this.props.className, selectedTabClassName)} onClick={this.handleTabChange.bind(this)}>
                    <div className="name">
                        {this.props.label}
                    </div>
                    <div className={notificationsClassName}>
                        <div className="circle">
                            {this.props.notifications}
                        </div>
                    </div>
                </div>
        );
    }
}

Tab.propTypes = {
    handleTabChange: React.PropTypes.func
};

Tab.defaultProps = {};