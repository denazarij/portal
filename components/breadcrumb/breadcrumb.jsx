"use strict";

require("./breadcrumb.scss");

var React = require("react");

import Item from "../../components/breadcrumb/item.jsx";

export default class BreadCrumb extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let itemsCount = this.props.children.length;
        let items = React.Children.map(this.props.children, (item, index) => {
            if (item.type == Item) {
                if ((itemsCount-1) === index) {
                    return React.cloneElement(item, {
                        key: 'item-'+index,
                        itemIndex: index,
                        selected: true});
                } else {
                    return React.cloneElement(item, {
                        key: 'item-'+index,
                        itemIndex: index});
                }
            } else {
                if(process.env.NODE_ENV !== "production") {
                    throw 'Breadcrumb only accepts Item Components as children. Found ' +
                    item.type + ' as child number ' + (index + 1) + ' of Breadcrumb';
                }
            }
        }, this);

        return (
            <div className="breadcrumb-wrapper">
                {items}
            </div>  
        );
    }
}
    
