"use strict";

var React = require("react");
var classNames = require('classnames');

export default class Item extends React.Component {
    constructor(props) {
        super(props);
    }

    handleItemChange() {
        this.props.onClick();
    }

    render() {
        if (this.props.selectedId) {
            return (
                <div className="item selected">{this.props.label}</div>
            );
        } else {
            return (
                <a className="item" onClick={this.handleItemChange.bind(this)}>{this.props.label}</a>
            );
        }
    }
}

Item.propTypes = {
};

Item.defaultProps = {};