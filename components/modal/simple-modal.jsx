// inspired from https://github.com/zackify/simple-react-modal

import React from 'react';
require("./modal.scss");

export default class Modal extends React.Component {

    constructor(props) {
        super(props);
    }

    hideOnOuterClick(event) {
        if (event.target.dataset.modal && this.props.onClose)
            this.props.onClose(event);
    }

    render() {
        if (!this.props.show)
            return null;
        else {
            return (
                <div {...this.props} className="modal-overlay"
                                     onClick={this.hideOnOuterClick.bind(this)}
                                     data-modal="true">
                    <div className="modal-container">
                        {this.props.children}
                    </div>
                </div>
            )
        }
    }
}

Modal.propTypes = {
    show: React.PropTypes.bool,
    onClose: React.PropTypes.func
};

